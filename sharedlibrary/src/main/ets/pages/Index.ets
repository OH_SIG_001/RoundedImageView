/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { PictureItem } from './PictureItem'
import { TypeCustomDialog } from './TypeCustomDialog'
import { RoundedImageView,RoundedImageName, ScaleType, TileMode, SrcType, FileUtils, GlobalContext } from '@ohos/roundedimageview'
import common from '@ohos.app.ability.common'
import display from '@ohos.display'

@Entry
@ComponentV2
struct Index {
  private picIdxArr: number [] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
  private colorIdxArr: number [] = [0, 1, 2, 3, 4, 5, 6]
  private uiWidth: number = px2vp(display.getDefaultDisplaySync().width * 0.9)
  private uiHeight: number = px2vp(500)

  private uriFolder: string = getContext(this).filesDir + "/" + "uriFolder"
  private uriFile: string = this.uriFolder + "/" + "photo1.jpg"
  private viewModels: RoundedImageName.Model [] = [];
  private scroller: Scroller = new Scroller()
  private rectPictureItems: PictureItem [] = [
    {
      src: $r('app.media.photo1'),
      srcType: SrcType.MEDIA,
      isSvg: false,
      primaryTitle: 'Tufa at night',
      secondTitle: 'Mono Lake, CA',
      scaleTypeName: 'CENTER',
      scaleType: ScaleType.CENTER,
      tileMode: null,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor: '',
      cornerRadius: 25,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 0
    },
    {
      src: $r('app.media.photo2'),
      srcType: SrcType.MEDIA,
      isSvg: false,
      primaryTitle: 'Starry night',
      secondTitle: 'Lake Powell, AZ',
      scaleTypeName: 'CENTER_CROP',
      scaleType: ScaleType.CENTER_CROP,
      tileMode: null,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor:'',
      cornerRadius: 25,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 0
    },
    {
      src: $r('app.media.photo3'),
      srcType: SrcType.MEDIA,
      isSvg: false,
      primaryTitle: 'Racetrack playa',
      secondTitle: 'Death Valley, CA',
      scaleTypeName: 'CENTER_INSIDE',
      scaleType: ScaleType.CENTER_INSIDE,
      tileMode: null,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor: '',
      cornerRadius: 25,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 0
    },
    {
      src: $r('app.media.photo4'),
      srcType: SrcType.MEDIA,
      isSvg: false,
      primaryTitle: 'Napali coast',
      secondTitle: 'Kauai, HI',
      scaleTypeName: 'FIT_CENTER',
      scaleType: ScaleType.FIT_CENTER,
      tileMode: null,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor: '',
      cornerRadius: 25,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 0
    },
    {
      src: $r('app.media.photo5'),
      srcType: SrcType.MEDIA,
      isSvg: false,
      primaryTitle: 'Delicate Arch',
      secondTitle: 'Arches, UT',
      scaleTypeName: 'FIT_END',
      scaleType: ScaleType.FIT_END,
      tileMode: null,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor: '',
      cornerRadius: 25,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 0
    },
    {
      src: $r('app.media.photo6'),
      srcType: SrcType.MEDIA,
      isSvg: false,
      primaryTitle: 'Sierra sunset',
      secondTitle: 'Lone Pine, CA',
      scaleTypeName: 'FIT_START',
      scaleType: ScaleType.FIT_START,
      tileMode: null,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor: '',
      cornerRadius: 25,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 0
    },
    {
      src: $r('app.media.photo7'),
      srcType: SrcType.MEDIA,
      isSvg: false,
      primaryTitle: 'Majestic',
      secondTitle: 'Grand Teton, WY',
      scaleTypeName: 'FIT_XY',
      scaleType: ScaleType.FIT_XY,
      tileMode: null,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor: '',
      cornerRadius: 25,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 0
    },
    {
      src: $r('app.media.black_white_tile'),
      srcType: SrcType.MEDIA,
      isSvg: false,
      primaryTitle: 'TileMode',
      secondTitle: 'REPEAT',
      scaleTypeName: 'FIT_XY',
      scaleType: ScaleType.FIT_XY,
      tileMode: TileMode.REPEAT,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor: '',
      cornerRadius: 25,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 0
    },
    {
      src: $r('app.media.black_white_tile'),
      srcType: SrcType.MEDIA,
      isSvg: false,
      primaryTitle: 'TileMode',
      secondTitle: 'CLAMP',
      scaleTypeName: 'FIT_XY',
      scaleType: ScaleType.FIT_XY,
      tileMode: TileMode.CLAMP,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor: '',
      cornerRadius: 25,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 0
    },
    {
      src: $r('app.media.black_white_tile'),
      srcType: SrcType.MEDIA,
      isSvg: false,
      primaryTitle: 'TileMode',
      secondTitle: 'MIRROR',
      scaleTypeName: 'FIT_XY',
      scaleType: ScaleType.FIT_XY,
      tileMode: TileMode.MIRROR,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor: '',
      cornerRadius: 25,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 0
    }
  ]
  private ovalPictureItems: PictureItem [] = [
    {
      src: 'photo1.jpg',
      srcType: SrcType.RAWFILE,
      isSvg: false,
      primaryTitle: 'Tufa at night',
      secondTitle: 'Mono Lake, CA',
      scaleTypeName: 'CENTER',
      scaleType: ScaleType.CENTER,
      tileMode: null,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor: '',
      cornerRadius: 0,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 0
    },
    {
      src: 'photo2.jpg',
      srcType: SrcType.RAWFILE,
      isSvg: false,
      primaryTitle: 'Starry night',
      secondTitle: 'Lake Powell, AZ',
      scaleTypeName: 'CENTER_CROP',
      scaleType: ScaleType.CENTER_CROP,
      tileMode: null,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor: '',
      cornerRadius: 0,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 0
    },
    {
      src: 'photo3.jpg',
      srcType: SrcType.RAWFILE,
      isSvg: false,
      primaryTitle: 'Racetrack playa',
      secondTitle: 'Death Valley, CA',
      scaleTypeName: 'CENTER_INSIDE',
      scaleType: ScaleType.CENTER_INSIDE,
      tileMode: null,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor: '',
      cornerRadius: 0,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 0
    },
    {
      src: 'photo4.jpg',
      srcType: SrcType.RAWFILE,
      isSvg: false,
      primaryTitle: 'Napali coast',
      secondTitle: 'Kauai, HI',
      scaleTypeName: 'FIT_CENTER',
      scaleType: ScaleType.FIT_CENTER,
      tileMode: null,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor: '',
      cornerRadius: 0,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 0
    },
    {
      src: 'photo5.jpg',
      srcType: SrcType.RAWFILE,
      isSvg: false,
      primaryTitle: 'Delicate Arch',
      secondTitle: 'Arches, UT',
      scaleTypeName: 'FIT_END',
      scaleType: ScaleType.FIT_END,
      tileMode: null,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor: '',
      cornerRadius: 0,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 0
    },
    {
      src: 'photo6.jpg',
      srcType: SrcType.RAWFILE,
      isSvg: false,
      primaryTitle: 'Sierra sunset',
      secondTitle: 'Lone Pine, CA',
      scaleTypeName: 'FIT_START',
      scaleType: ScaleType.FIT_START,
      tileMode: null,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor: '',
      cornerRadius: 0,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 0
    },
    {
      src: 'photo7.jpg',
      srcType: SrcType.RAWFILE,
      isSvg: false,
      primaryTitle: 'Majestic',
      secondTitle: 'Grand Teton, WY',
      scaleTypeName: 'FIT_XY',
      scaleType: ScaleType.FIT_XY,
      tileMode: null,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor: '',
      cornerRadius: 0,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 0
    },
    {
      src: 'black_white_tile.jpg',
      srcType: SrcType.RAWFILE,
      isSvg: false,
      primaryTitle: 'TileMode',
      secondTitle: 'REPEAT',
      scaleTypeName: 'FIT_XY',
      scaleType: ScaleType.FIT_XY,
      tileMode: TileMode.REPEAT,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor: '',
      cornerRadius: 0,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 0
    },
    {
      src: 'black_white_tile.jpg',
      srcType: SrcType.RAWFILE,
      isSvg: false,
      primaryTitle: 'TileMode',
      secondTitle: 'CLAMP',
      scaleTypeName: 'FIT_XY',
      scaleType: ScaleType.FIT_XY,
      tileMode: TileMode.CLAMP,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor: '',
      cornerRadius: 0,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 0
    },
    {
      src: 'black_white_tile.jpg',
      srcType: SrcType.RAWFILE,
      isSvg: false,
      primaryTitle: 'TileMode',
      secondTitle: 'MIRROR',
      scaleTypeName: 'FIT_XY',
      scaleType: ScaleType.FIT_XY,
      tileMode: TileMode.MIRROR,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor: '',
      cornerRadius: 0,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 0
    }
  ]
  private colorItems: PictureItem [] = [
    {
      src: '',
      srcType: null,
      isSvg: false,
      primaryTitle: 'Color',
      secondTitle: '',
      scaleTypeName: 'CENTER',
      scaleType: ScaleType.CENTER,
      tileMode: null,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor: '#AAAAAA',
      cornerRadius: 25,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 0
    },
    {
      src: '',
      srcType: null,
      isSvg: false,
      primaryTitle: 'Color',
      secondTitle: '',
      scaleTypeName: 'CENTER_CROP',
      scaleType: ScaleType.CENTER_CROP,
      tileMode: null,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor: '#FF8800',
      cornerRadius: 25,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 0
    },
    {
      src: '',
      srcType: null,
      isSvg: false,
      primaryTitle: 'Color',
      secondTitle: '',
      scaleTypeName: 'CENTER_INSIDE',
      scaleType: ScaleType.CENTER_INSIDE,
      tileMode: null,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor: '#FAFAFA',
      cornerRadius: 25,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 0
    },
    {
      src: '',
      srcType: null,
      isSvg: false,
      primaryTitle: 'Color',
      secondTitle: '',
      scaleTypeName: 'FIT_CENTER',
      scaleType: ScaleType.FIT_CENTER,
      tileMode: null,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor: '#669900',
      cornerRadius: 25,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 0
    },
    {
      src: '',
      srcType: null,
      isSvg: false,
      primaryTitle: 'Color',
      secondTitle: '',
      scaleTypeName: 'FIT_END',
      scaleType: ScaleType.FIT_END,
      tileMode: null,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor: '#CC0000',
      cornerRadius: 25,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 0
    },
    {
      src: '',
      srcType: null,
      isSvg: false,
      primaryTitle: 'Color',
      secondTitle: '',
      scaleTypeName: 'FIT_START',
      scaleType: ScaleType.FIT_START,
      tileMode: null,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor: '#AA66CC',
      cornerRadius: 25,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 0
    },
    {
      src: '',
      srcType: null,
      isSvg: false,
      primaryTitle: 'Color',
      secondTitle: '',
      scaleTypeName: 'FIT_XY',
      scaleType: ScaleType.FIT_XY,
      tileMode: null,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor: '#FFFFFF',
      cornerRadius: 25,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 0
    },
  ]
  private backgroundColorItems: PictureItem [] = [
    {
      src: 'https://hbimg.huabanimg.com/0ef60041445edcfd6b38d20e19024b2cd9281dcc3525a4-Vy8fYO_fw658/format/webp',
      srcType: SrcType.URL,
      isSvg: false,
      primaryTitle: 'Tufa at night',
      secondTitle: 'Mono Lake, CA',
      scaleTypeName: 'CENTER',
      scaleType: ScaleType.CENTER,
      tileMode: null,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor: '#55AA66',
      cornerRadius: 25,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 30
    },
    {
      src: 'https://hbimg.huabanimg.com/0ef60041445edcfd6b38d20e19024b2cd9281dcc3525a4-Vy8fYO_fw658/format/webp',
      srcType: SrcType.URL,
      isSvg: false,
      primaryTitle: 'Starry night',
      secondTitle: 'Lake Powell, AZ',
      scaleTypeName: 'CENTER_CROP',
      scaleType: ScaleType.CENTER_CROP,
      tileMode: null,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor: '#55AA66',
      cornerRadius: 25,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 30
    },
    {
      src: this.uriFile,
      srcType: SrcType.URI,
      isSvg: false,
      primaryTitle: 'Racetrack playa',
      secondTitle: 'Death Valley, CA',
      scaleTypeName: 'CENTER_INSIDE',
      scaleType: ScaleType.CENTER_INSIDE,
      tileMode: null,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor: '#55AA66',
      cornerRadius: 25,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 30
    },
    {
      src: this.uriFile,
      srcType: SrcType.URI,
      isSvg: false,
      primaryTitle: 'Napali coast',
      secondTitle: 'Kauai, HI',
      scaleTypeName: 'FIT_CENTER',
      scaleType: ScaleType.FIT_CENTER,
      tileMode: null,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor: '#55AA66',
      cornerRadius: 25,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 30
    },
    {
      src: $r('app.media.photo5'),
      srcType: SrcType.MEDIA,
      isSvg: false,
      primaryTitle: 'Delicate Arch',
      secondTitle: 'Arches, UT',
      scaleTypeName: 'FIT_END',
      scaleType: ScaleType.FIT_END,
      tileMode: null,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor: '#55AA66',
      cornerRadius: 25,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 30
    },
    {
      src: $r('app.media.photo6'),
      srcType: SrcType.MEDIA,
      isSvg: false,
      primaryTitle: 'Sierra sunset',
      secondTitle: 'Lone Pine, CA',
      scaleTypeName: 'FIT_START',
      scaleType: ScaleType.FIT_START,
      tileMode: null,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor: '#55AA66',
      cornerRadius: 25,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 30
    },
    {
      src: $r('app.media.photo7'),
      srcType: SrcType.MEDIA,
      isSvg: false,
      primaryTitle: 'Majestic',
      secondTitle: 'Grand Teton, WY',
      scaleTypeName: 'FIT_XY',
      scaleType: ScaleType.FIT_XY,
      tileMode: null,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor: '#55AA66',
      cornerRadius: 25,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 30
    },
    {
      src: $r('app.media.black_white_tile'),
      srcType: SrcType.MEDIA,
      isSvg: false,
      primaryTitle: 'TileMode',
      secondTitle: 'REPEAT',
      scaleTypeName: 'FIT_XY',
      scaleType: ScaleType.FIT_XY,
      tileMode: TileMode.REPEAT,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor: '#55AA66',
      cornerRadius: 25,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 30
    },
    {
      src: $r('app.media.black_white_tile'),
      srcType: SrcType.MEDIA,
      isSvg: false,
      primaryTitle: 'TileMode',
      secondTitle: 'CLAMP',
      scaleTypeName: 'FIT_XY',
      scaleType: ScaleType.FIT_XY,
      tileMode: TileMode.CLAMP,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor: '#55AA66',
      cornerRadius: 25,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 30
    },
    {
      src: $r('app.media.black_white_tile'),
      srcType: SrcType.MEDIA,
      isSvg: false,
      primaryTitle: 'TileMode',
      secondTitle: 'MIRROR',
      scaleTypeName: 'FIT_XY',
      scaleType: ScaleType.FIT_XY,
      tileMode: TileMode.MIRROR,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor: '#55AA66',
      cornerRadius: 25,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 30
    }
  ]
  private svgItems: PictureItem [] = [
    {
      src: $r('app.media.ic_svg'),
      srcType: SrcType.MEDIA,
      isSvg: true,
      primaryTitle: 'SVG',
      secondTitle: '',
      scaleTypeName: 'CENTER',
      scaleType: ScaleType.CENTER,
      tileMode: null,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor: '',
      cornerRadius: 25,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 0
    },
    {
      src: $r('app.media.ic_svg'),
      srcType: SrcType.MEDIA,
      isSvg: true,
      primaryTitle: 'SVG',
      secondTitle: '',
      scaleTypeName: 'CENTER_CROP',
      scaleType: ScaleType.CENTER_CROP,
      tileMode: null,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor: '',
      cornerRadius: 25,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 0
    },
    {
      src: $r('app.media.ic_svg'),
      srcType: SrcType.MEDIA,
      isSvg: true,
      primaryTitle: 'SVG',
      secondTitle: '',
      scaleTypeName: 'CENTER_INSIDE',
      scaleType: ScaleType.CENTER_INSIDE,
      tileMode: null,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor: '',
      cornerRadius: 25,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 0
    },
    {
      src: 'ic_svg.svg',
      srcType: SrcType.RAWFILE,
      isSvg: true,
      primaryTitle: 'SVG',
      secondTitle: '',
      scaleTypeName: 'FIT_CENTER',
      scaleType: ScaleType.FIT_CENTER,
      tileMode: null,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor: '',
      cornerRadius: 25,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 0
    },
    {
      src: 'ic_svg.svg',
      srcType: SrcType.RAWFILE,
      isSvg: true,
      primaryTitle: 'SVG',
      secondTitle: '',
      scaleTypeName: 'FIT_END',
      scaleType: ScaleType.FIT_END,
      tileMode: null,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor: '',
      cornerRadius: 25,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 0
    },
    {
      src: 'ic_svg.svg',
      srcType: SrcType.RAWFILE,
      isSvg: true,
      primaryTitle: 'SVG',
      secondTitle: '',
      scaleTypeName: 'FIT_START',
      scaleType: ScaleType.FIT_START,
      tileMode: null,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor: '',
      cornerRadius: 25,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 0
    },
    {
      src: 'ic_svg.svg',
      srcType: SrcType.RAWFILE,
      isSvg: true,
      primaryTitle: 'SVG',
      secondTitle: '',
      scaleTypeName: 'FIT_XY',
      scaleType: ScaleType.FIT_XY,
      tileMode: null,
      uiWidth: this.uiWidth,
      uiHeight: this.uiHeight,
      backgroundColor: '',
      cornerRadius: 25,
      borderWidth: 10,
      borderColor: '#317AF7',
      padding: 0
    },
  ]
  @Local typeValue: string = 'Bitmap'
  @Monitor("typeValue")
  private typeValueChanged(): void {
    if (this.typeValue == 'Bitmap') {
      this.updateViewModels(this.rectPictureItems)
    } else if (this.typeValue == 'Ovals') {
      this.updateViewModels(this.ovalPictureItems)
    } else if (this.typeValue == 'Color') {
      this.updateViewModels(this.colorItems)
    } else if (this.typeValue == 'Background') {
      this.updateViewModels(this.backgroundColorItems)
    } else if (this.typeValue == 'SVG') {
      this.updateViewModels(this.svgItems)
    }
    this.scroller.scrollTo({ xOffset: 0, yOffset: 0, animation: { duration: 2000, curve: Curve.Ease } })
  }
  aboutToAppear() {
    this.initViewModels()
    this.typeValueChanged()
    this.initUri()
  }

  private initViewModels(): void  {
    let viewModelsLength = Math.max(this.picIdxArr.length, this.colorIdxArr.length)
    for (let index = 0; index < viewModelsLength; index++) {
      this.viewModels.push(new RoundedImageName.Model)
    }
  }

  private updateViewModels(pictureItem: PictureItem[]) {
    pictureItem.forEach((val, idx) => {
      this.viewModels[idx]
        .setImageSrc(pictureItem[idx].src)
        .setBackgroundColor(pictureItem[idx].backgroundColor)
        .setSrcType(pictureItem[idx].srcType)
        .setIsSvg(pictureItem[idx].isSvg)
        .setTypeValue(this.typeValue)
        .setUiWidth(pictureItem[idx].uiWidth)
        .setUiHeight(pictureItem[idx].uiHeight)
        .setScaleType(pictureItem[idx].scaleType)
        .setTileModeXY(pictureItem[idx].tileMode)
        .setCornerRadius(pictureItem[idx].cornerRadius)
        .setBorderWidth(pictureItem[idx].borderWidth)
        .setBorderColor(pictureItem[idx].borderColor)
        .setPadding(pictureItem[idx].padding)
        .setColorWidth(this.uiHeight)
        .setColorHeight(this.uiHeight)
        .setContext(getContext(this).createModuleContext('sharedlibrary') as common.UIAbilityContext)
    });
  }

  private initUri() {
    FileUtils.getInstance().createFolder(this.uriFolder);
    (getContext(this).createModuleContext('sharedlibrary') as common.UIAbilityContext).resourceManager.getMedia($r('app.media.photo1').id, (error: Error, value: Uint8Array) => {
      FileUtils.getInstance().writePic(this.uriFile, this.uint8ArrayToBuffer(value))
    })
  }

  uint8ArrayToBuffer(array: Uint8Array): ArrayBuffer {
    return array.buffer.slice(array.byteOffset, array.byteLength + array.byteOffset)
  }

  build() {
    Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
      Column() {
        Column() {
          TypeCustomDialog({typeValue: this.typeValue!!})
          Text(' select:' + this.typeValue).fontSize(30)
        }.margin(15)

        Scroll(this.scroller) {
          List({ space: 10, initialIndex: 0 }) {
            if (this.typeValue == 'Bitmap') {
              ForEach(this.picIdxArr, (item:number) => {
                ListItem() {
                  this.ViewItem(this.viewModels[item], this.rectPictureItems[item])
                }.editable(false)
              }, (item:string) => item)
            } else if (this.typeValue == 'Ovals') {
              ForEach(this.picIdxArr, (item:number) => {
                ListItem() {
                  this.ViewItem(this.viewModels[item], this.ovalPictureItems[item])
                }.editable(false)
              }, (item:string) => item)
            } else if (this.typeValue == 'Color') {
              ForEach(this.colorIdxArr, (item:number) => {
                ListItem() {
                  this.ViewItem(this.viewModels[item], this.colorItems[item])
                }.editable(false)
              }, (item:string) => item)
            } else if (this.typeValue == 'Background') {
              ForEach(this.picIdxArr, (item:number) => {
                ListItem() {
                  this.ViewItem(this.viewModels[item], this.backgroundColorItems[item])
                }.editable(false)
              }, (item:string) => item)
            } else if (this.typeValue == 'SVG') {
              ForEach(this.colorIdxArr, (item:number) => {
                ListItem() {
                  this.ViewItem(this.viewModels[item], this.svgItems[item])
                }.editable(false)
              }, (item:string) => item)
            }
          }
        }
        .scrollable(ScrollDirection.Vertical).scrollBar(BarState.Off)
      }
      .width('100%')
      .height('100%')
      .backgroundColor(0xDCDCDC)
      .padding({ top: 20, bottom: 100 })
    }
  }

  @Builder ViewItem(roundedImageViewModel: RoundedImageName.Model, pictureItem: PictureItem) {
    Column({ space: 5 }) {
      RoundedImageView({ model: roundedImageViewModel })
      Column() {
        Text(pictureItem.primaryTitle)
          .size({ height: 35 })
          .fontColor(0xFAFAFA)
          .backgroundColor(0x7f000000)
          .fontSize(18)
          .position({ x: 20, y: 40 })
        Text(pictureItem.secondTitle)
          .size({ height: 20 })
          .fontColor(0xFAFAFA)
          .backgroundColor(0x7f000000)
          .fontSize(16)
          .position({ x: 20, y: 90 })
        Text(pictureItem.scaleTypeName)
          .size({ height: 20 })
          .fontColor(0xFAFAFA)
          .backgroundColor(0x7f000000)
          .fontSize(14)
          .position({ x: 20, y: 120 })
      }
    }.width('100%')
  }
}
